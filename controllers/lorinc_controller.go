/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	helmclient "github.com/mittwald/go-helm-client"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	goacademytsystemscomv1 "gitlab.com/viliaml/operator/api/v1"
)

// LorincReconciler reconciles a Lorinc object
type LorincReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

const myFinalizer = "vlorinc-operator/finalizer"

//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=lorincs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=lorincs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=lorincs/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Lorinc object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *LorincReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	logger.Info("Reconcile start")
	result := ctrl.Result{}
	// TODO(user): your logic here
	var test goacademytsystemscomv1.Lorinc
	err := r.Client.Get(ctx, req.NamespacedName, &test)
	if err != nil {
		return result, client.IgnoreNotFound(err)
	}

	opt := &helmclient.Options{
		Namespace: test.Spec.Namespace, // Change this to the namespace you wish the client to operate in.
		Debug:     true,
		Linting:   false,
		DebugLog:  func(format string, v ...interface{}) {},
	}

	helmClient, err := helmclient.New(opt)
	if err != nil {
		return result, err
	}

	chartSpec := helmclient.ChartSpec{
		ReleaseName:     test.Name,
		ChartName:       test.Spec.HelmChartPath,
		Namespace:       test.Spec.Namespace,
		Wait:            true,
		Timeout:         5 * time.Minute,
		CreateNamespace: true,
	}

	if !test.DeletionTimestamp.IsZero() {
		return r.UninstallHelm(logger, helmClient, ctx, chartSpec, &test)
	} else if test.Spec.HelmChartPath != test.Status.InstalledHelmChart {
		err = r.InstallHelm(logger, helmClient, ctx, chartSpec, &test)
		if err == nil {
			result.Requeue = true
		}
	} else if !test.Spec.Restart.Equal(&test.Status.Restart) && test.Generation > 1 {
		return r.Restart(logger, ctx, test)

	}

	return result, err
}

// SetupWithManager sets up the controller with the Manager.
func (r *LorincReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&goacademytsystemscomv1.Lorinc{}).
		WithEventFilter(predicate.GenerationChangedPredicate{}).
		Complete(r)

}

func (r *LorincReconciler) InstallHelm(logger logr.Logger, helmClient helmclient.Client, ctx context.Context, chartSpec helmclient.ChartSpec, test *goacademytsystemscomv1.Lorinc) error {
	logger.Info("INSTALL")
	// Install a chart release.
	// Note that helmclient.Options.Namespace should ideally match the namespace in chartSpec.Namespace.
	if _, err := helmClient.InstallOrUpgradeChart(ctx, &chartSpec, nil); err != nil {
		return err
	}

	patch := client.MergeFrom(test.DeepCopy())
	fmt.Println(test.Finalizers)

	if !controllerutil.ContainsFinalizer(test, myFinalizer) {
		controllerutil.AddFinalizer(test, myFinalizer)
		fmt.Println(test.Finalizers)
	}
	err := r.Patch(ctx, test, patch)
	if err != nil {
		return err
	}

	patch = client.MergeFrom(test.DeepCopy())
	test.Status.InstalledHelmChart = test.Spec.HelmChartPath
	err = r.Status().Patch(ctx, test, patch)
	if err != nil {
		return err
	}
	return nil
}

func (r *LorincReconciler) UninstallHelm(logger logr.Logger, helmClient helmclient.Client, ctx context.Context, chartSpec helmclient.ChartSpec, test *goacademytsystemscomv1.Lorinc) (ctrl.Result, error) {

	logger.Info("UNINSTALL")
	// uninstall Helm chart
	if err := helmClient.UninstallRelease(&chartSpec); err != nil {
		return ctrl.Result{}, err
	}

	patch := client.MergeFrom(test.DeepCopy())
	fmt.Println(test.Finalizers)

	if controllerutil.ContainsFinalizer(test, myFinalizer) {

		controllerutil.RemoveFinalizer(test, myFinalizer)
		fmt.Println(test.Finalizers)
		if err := r.Patch(ctx, test, patch); err != nil {
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

func (r *LorincReconciler) Restart(logger logr.Logger, ctx context.Context, test goacademytsystemscomv1.Lorinc) (ctrl.Result, error) {
	logger.Info("RESTART")
	var deploy appsv1.Deployment
	name := test.Name + "-helm-gin-server"
	err := r.Client.Get(ctx, types.NamespacedName{Name: name, Namespace: test.Spec.Namespace}, &deploy)
	if err != nil {
		return ctrl.Result{}, err
	}
	if deploy.Spec.Template.ObjectMeta.Annotations == nil {
		deploy.Spec.Template.ObjectMeta.Annotations = make(map[string]string)
	}
	deploy.Spec.Template.ObjectMeta.Annotations["restartAt"] = time.Now().Format(time.RFC3339)
	err2 := r.Client.Update(ctx, &deploy)
	if err2 != nil {
		return ctrl.Result{}, err2
	}

	patch := client.MergeFrom(test.DeepCopy())
	test.Status.Restart = test.Spec.Restart

	if err := r.Status().Patch(ctx, &test, patch); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}
