/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var lorinclog = logf.Log.WithName("lorinc-resource")

func (r *Lorinc) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!

//+kubebuilder:webhook:path=/mutate-goacademy-t-systems-com-v1-lorinc,mutating=true,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=lorincs,verbs=create;update;delete,versions=v1,name=mlorinc.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &Lorinc{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *Lorinc) Default() {
	lorinclog.Info("default", "name", r.Name)

	// TODO(user): fill in your defaulting logic.
	if r.Spec.HelmChartPath == "" {
		r.Spec.HelmChartPath = "/tmp/helm-gin-server-0.1.0.tgz"
	}
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-goacademy-t-systems-com-v1-lorinc,mutating=false,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=lorincs,verbs=create;update;delete,versions=v1,name=vlorinc.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &Lorinc{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Lorinc) ValidateCreate() error {
	lorinclog.Info("validate create", "name", r.Name)

	return r.CheckIfFile()
	// TODO(user): fill in your validation logic upon object creation.
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Lorinc) ValidateUpdate(old runtime.Object) error {
	lorinclog.Info("validate update", "name", r.Name)

	// TODO(user): fill in your validation logic upon object update.
	return r.CheckIfFile()
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Lorinc) ValidateDelete() error {
	lorinclog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}

func (r *Lorinc) CheckIfFile() error {
	if f, err := os.Stat(r.Spec.HelmChartPath); err != nil {
		lorinclog.Info("HelmChart File does not exists\n")
		return err
	} else if !f.Mode().IsRegular() {
		lorinclog.Info("HelmChart is not file\n")
		return err
	}
	return nil
}
